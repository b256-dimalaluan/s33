console.log('Activity');


// Create a fetch request using the GET method that will retrieve ALL the to do list items from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => console.log(json));

//Using the data retrieved, create an array using the map method to return ONLY the title of every item and print the result in the console.
let titles = [];

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => {
      let fields = json;
      fields.map(function(id) {
      titles.push(id.title);
    });
})

console.log(titles);
// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json));

// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => {
	let id = json.id;
    let title = json.title;
    let status = json.completed;
    console.log(`The item ${id}: \"${title}\" has a status of \"${status}\"`);
})






//Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos", {
	
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	
	body: JSON.stringify({
		title: "Magsaing",
		completed: false,
		userId: 1,
		id: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {'Content-Type': 'application/json'},
	body: JSON. stringify({
		title: "Magworkout",
		completed: true,
		userId: 2,
		id: 2
	})
})
.then(response => response.json())
.then(json => console.log(json))
 
/*
 Update a to do list item by changing the data structure to contain the following properties:
Title
Description
Status
Date Completed
User ID
*/

fetch("https://jsonplaceholder.typicode.com/todos/66", {
	method: "PUT",
	headers: {'Content-Type': 'application/json'},
	body: JSON. stringify({
		title: "Maggawa ng activity",
		description: "Gawain",
		status: false,
		date_completed: 03/29/2023,
		userId: 2
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/18", {
	method: "PUT",
	headers: {'Content-Type': 'application/json'},
	body: JSON. stringify({
		title: "Makinig sa class",
		completed: true,
		userId: 18,
		id: 18
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch("https://jsonplaceholder.typicode.com/todos/32", {
	method: "PUT",
	headers: {'Content-Type': 'application/json'},
	body: JSON. stringify({
		title: "Magluto ng ulam",
		status: "complete",
		date: "3/29/2023"
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder AP

fetch("https://jsonplaceholder.typicode.com/todos/16", {
	method: "DELETE"
});




